require("dotenv").config({
  path: `.env.${process.env.NODE_ENV}`,
});

module.exports = {
  siteMetadata: {
    siteUrl: "https://www.yourdomain.tld",
    title: "Diccionario Ilógica",
  },
  plugins: [
    "gatsby-plugin-netlify-cms",
    "gatsby-plugin-sass",
    "gatsby-plugin-image",
    {
      resolve: `gatsby-plugin-google-gtag`,
      options: {
        // You can add multiple tracking ids and a pageview event will be fired for all of them.
        trackingIds: [
          "G-BR4JNWBQ8L", // Google Analytics / GA
          "", // Google Ads / Adwords / AW
          "", // Marketing Platform advertising products (Display & Video 360, Search Ads 360, and Campaign Manager)
        ],
        // This object gets passed directly to the gtag config command
        // This config will be shared across all trackingIds
        gtagConfig: {
          optimize_id: "OPT_CONTAINER_ID",
          anonymize_ip: true,
          cookie_expires: 0,
        },
        // This object is used for configuration specific to this plugin
        pluginConfig: {
          // Puts tracking script in the head instead of the body
          head: false,
          // Setting this parameter is also optional
          respectDNT: true,
          // Avoids sending pageview hits from custom paths
          exclude: ["/preview/**", "/do-not-track/me/too/"],
        },
      },
    },
    // {
    //   resolve: "gatsby-plugin-google-analytics",
    //   options: {
    //     trackingId: "G-BR4JNWBQ8L",
    //   },
    // },
    // {
    //   resolve: "gatsby-plugin-google-tagmanager",
    //   options: {
    //     id: "G-BR4JNWBQ8L",

    //     // Include GTM in development.
    //     //
    //     // Defaults to false meaning GTM will only be loaded in production.
    //     includeInDevelopment: false,

    //     // datalayer to be set before GTM is loaded
    //     // should be an object or a function that is executed in the browser
    //     //
    //     // Defaults to null
    //     defaultDataLayer: { platform: "gatsby" },

    //     // Specify optional GTM environment details.
    //     // gtmAuth: "YOUR_GOOGLE_TAGMANAGER_ENVIRONMENT_AUTH_STRING",
    //     // gtmPreview: "YOUR_GOOGLE_TAGMANAGER_ENVIRONMENT_PREVIEW_NAME",
    //     // dataLayerName: "YOUR_DATA_LAYER_NAME",

    //     // Name of the event that is triggered
    //     // on every Gatsby route change.
    //     //
    //     // Defaults to gatsby-route-change
    //     // routeChangeEventName: "YOUR_ROUTE_CHANGE_EVENT_NAME",
    //     // Defaults to false
    //     enableWebVitalsTracking: true,
    //     // Defaults to https://www.googletagmanager.com
    //     // selfHostedOrigin: "YOUR_SELF_HOSTED_ORIGIN",
    //   },
    // },
    "gatsby-plugin-react-helmet",
    "gatsby-plugin-sitemap",
    {
      resolve: "gatsby-plugin-manifest",
      options: {
        icon: "src/images/icon.png",
      },
    },
    "gatsby-plugin-mdx",
    "gatsby-plugin-sharp",
    "gatsby-transformer-sharp",
    {
      resolve: "gatsby-source-filesystem",
      options: {
        name: "images",
        path: "./src/images/",
      },
      __key: "images",
    },
    {
      resolve: "gatsby-source-filesystem",
      options: {
        name: "pages",
        path: "./src/pages/",
      },
      __key: "pages",
    },
    {
      resolve: "gatsby-source-google-spreadsheet",
      options: {
        // The `spreadsheetId` is required, it is found in the url of your document:
        // https://docs.google.com/spreadsheets/d/<spreadsheetId>/edit#gid=0
        spreadsheetId: "1zZnuZIhcHKzM922E5U7ekQ0BYRpBdcTXMVUnaq_IDf8",
        // The `spreadsheetName` is recommended, but optional
        // It is used as part of the id's during the node creation, as well as in the generated GraphQL-schema
        // If you are sourcing multiple sheets, you can set this to distringuish between the source data
        spreadsheetName: "DiccionarioIlogico",

        // The `typePrefix` is optional, default value is "GoogleSpreadsheet"
        // It is used as part of the id's during the node creation, as well as in the generated GraphQL-schema
        // It can be overridden to fully customize the root query
        typePrefix: "GoogleSpreadsheet",

        // The `credentials` are only needed when you need to be authenticated to read the document.
        // It's an object with the following shape:
        // {
        //   client_email: "<your service account email address>",
        //   private_key: "<the prive key for your service account>"
        // }
        //
        // Refer to googles own documentation to retrieve the credentials for your service account:
        //   - https://github.com/googleapis/google-api-nodejs-client#service-to-service-authentication
        //   - https://developers.google.com/identity/protocols/OAuth2ServiceAccount
        //
        // When you have generated your credentials, it's easiest to refer to them from an environment variable
        // and parse it directly:
        credentials: {
          type: "service_account",
          project_id: "diccionario-ilogico",
          private_key_id: "db572eab26d11523bd49de8794d9e81c918d66d6",
          private_key:
            "-----BEGIN PRIVATE KEY-----\nMIIEvgIBADANBgkqhkiG9w0BAQEFAASCBKgwggSkAgEAAoIBAQDU3+FaXJRoTc0T\nuCtK0gZhg5iL9g29vkH9GYEykU26BgEEt0NRA54PKLh5oOJsLSHKpJop7C1o5I5w\nExjaMzJGUVQVm0HjZdqkOb25xN/qw9nVsE7XrEPrUEDtdUvtEAdcEt9C8qkNpkN9\nA+H95chCIxVqkzAxUTxfulbfMfadeXaKQyhXTQ1GrBwVLW2krJGEMCgOraFd5rbn\nVnV9hmoVJmOx7Tw41CI2gjZVNesSj4Px+Bu6e6k4EojoQEBWTS0V8f5TZRHDBJQC\n1bPrhA2Hk+1Q+AgMQBeB64O1owE0lyn5lrGJavM8zDpAc/TlUOYohw9D/2z2Bm0/\nCaM7awyxAgMBAAECggEASyx6kOYDtKfQ5YdSpnvvq+0EaFyucpSnCgHBOcHNUIWB\nQ5CSMaBU52aKas1RWEFB3yVsmaRhTtrzoAlma9vLg8CBsJWg2XRlFzyazukC6A1c\ntlUqKoiXfZhcLV8QPnm8F5xEgoheAcd2qsW9v2MvaoeoLvgmeywEZYvQ1MX4oJgY\nwgzaCi90UBAn8vcfSOb5kHPcExuy3S1M12KzAS8ui+wcbTh5TfIcV6Qmz6bOKsq1\nCDglYrvMdYp2exUAFgqRL814PyjY1dJrtKHykcqrpKOfm4sjBKxXfo9DhKEtdHEr\nc+T6zMbrRB/UKoCSK1jlgQIVS71gvwkf5HukpZEjjwKBgQDvYC/o+BoV1NZ2aqO9\n7ZMDItv7VHsv+//8/wsCnGvtzG63vlUm8zhY8FK3lgKI3R3ITlQtib+k38Pav0iW\nmGe29+itW4qTE7QiknXUDThhcrjARqbr8c99BZAl7ZTmFRWml2esQxkX3uoyoAUn\nkz8XS2E9+SZTPtvCPjcE7cjPtwKBgQDjqIieidRyXrtZNVG1EZW80obV8f+3LQPK\nNbrQ/FLUbm382eIF29RAaI2tmBu96ZNLlnyBoR85h7CXGhy2H+SvQEI6DCfbxp+T\ng1lDV093jB97pue3Q5FZVF30L05HIS4qpH9lcgP2PtlcJhzt1LzGaJCeJArr1tZC\nUn/3S6w21wKBgQC4mFl589HR4hccrHHnJk1HjttLfsOcpl2rUVbBRbUpZYxAIAP0\njH14gKOZNZJwzuuHq3fYwjBKydB0YSxuDct1WspQ3ZYIg5pXStCYnTVMWLbGd/WD\nYC9cvyyBQyyfHcVbeciWHAw+z0WVkrdG5JMzuIqdywPWEFRR/75YC7LfEwKBgD5u\nFSkEuxGyEMZh7MouzFU/lmJNPsbIvWVSXqy4xS4kKl1Qkr7wjcAr685TDCiESTRN\nTE9wE9P+9do5+u2myVuVz5ODidqnGVOmjW/+HXi3IFsUZi7HhLBuhmVUI6811icz\nfQwgvB1nFsxCV2SRFkOCoUX8tFIAVDlTzmvOgKd5AoGBALmzb2heBTm6uF38bzRG\nbm7jVWPhQbX6ZAhjM2zSns9yktX4eXoqo/rAxgiTn3MstK48aLu5G1dvtC59I8Dx\n2zR6dF5xO7QRsbXB2TGvJuCLnSeqhZm3KCZEPaBjJ1AxVWiQAPQIAhnw0z/+oulT\nW7ZAmO/o7huP2JJp1pSc27rW\n-----END PRIVATE KEY-----\n",
          client_email:
            "dicionarioilogico@diccionario-ilogico.iam.gserviceaccount.com",
          client_id: "117026397561584270163",
          auth_uri: "https://accounts.google.com/o/oauth2/auth",
          token_uri: "https://oauth2.googleapis.com/token",
          auth_provider_x509_cert_url:
            "https://www.googleapis.com/oauth2/v1/certs",
          client_x509_cert_url:
            "https://www.googleapis.com/robot/v1/metadata/x509/dicionarioilogico%40diccionario-ilogico.iam.gserviceaccount.com",
        },

        // Simple node transformation during node sourcing can be achieved by implementing the following functions
        // - `filterNode`
        // - `mapNode`
        //
        // By implementing a `filterNode(node): boolean` function, you can choose to eliminate some nodes before
        // they're added to Gatsby, the default behaviour is to include all nodes:
        filterNode: () => true,

        // By implementing a `mapNode(node): node` function, you can provide your own node transformations directly
        // during node sourcing, the default implementation is to return the node as is:
        mapNode: (node) => node,
      },
    },
  ],
};
