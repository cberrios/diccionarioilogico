import React, { PureComponent } from 'react'

export default class ColaboraForm extends PureComponent {
  render() {
    return (
      <>
        <section className="colabora">
          <h2>Colabora</h2>
          <p>Enviar tu palabra favorita llenando el siguiente formulario: </p>

          <iframe src="https://docs.google.com/forms/d/e/1FAIpQLScM0qVcobsN_--OcFK3ybzaSGG3bhBaw06zhFt5HNg65Oj8fA/viewform?embedded=true" width="640" height="943" frameborder="0" marginheight="0" marginwidth="0">Cargando…</iframe>
        </section>
      </>
    )
  }
}
