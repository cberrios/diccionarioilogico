import React from "react"
import { useStaticQuery, graphql } from "gatsby"

const CountCL = () => {
  const data = useStaticQuery(graphql`
    {
      allGoogleSpreadsheetDiccionarioIlogicoRespuestasDeFormulario1(
        filter: {pa_s: {in: "Chile"}}
      ) {
        totalCount
      }
    }
  `)
  return <>{data.allGoogleSpreadsheetDiccionarioIlogicoRespuestasDeFormulario1.totalCount}</>
}

export default CountCL

