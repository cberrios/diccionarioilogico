import * as React from "react";
import "../styles/base.scss";
import BuildTime from "../components/BuildTime";
import { Helmet } from "react-helmet";
import WordCount from '../components/WordCount';
import ListColombia from '../components/ListColombia';
import CountryList from "../components/CountryList";

// markup
const IndexPage = () => {
  return (
    <>
      <Helmet>
        <title>Diccionario Ilógico - Colombia</title>
      </Helmet>
      <main className="container">
        <aside>
          <div className="content">
            <div className="intro">
              <h1>Diccionario Ilógico</h1>
              <p>L@s ilógic@s nos enseñamos palabras y modismos</p>
            </div>

            <CountryList />

            <a className="cta" rel="noreferrer" target="_blank" href="https://forms.gle/y3Z5M4RmATH3TrS5A">Envía tu palabra favorita</a>


            <p className="updated">
              Listando <WordCount /> palabras -
              Actualizado el <BuildTime/>
            </p>
          </div>
        </aside>
        <article>
          <ListColombia />
        </article>
      </main>
    </>
  );
};

export default IndexPage;
